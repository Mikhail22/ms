package com.example.service.email;

import com.example.models.EmailDto;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.stream.annotation.StreamListener;
//import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
//import ru.advantum.csptm.service.email.model.EmailDto;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class EmailService {
    private final JavaMailSender emailSender;
    private final EmailServiceProperties properties;

    @Autowired
    public EmailService(JavaMailSender sender, EmailServiceProperties properties) {
        this.emailSender = sender;
        this.properties = properties;
    }

//    @StreamListener(Sink.INPUT)
    public void consumeMessage(EmailDto mail) throws MessagingException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom(new InternetAddress(properties.getFrom()));
        helper.setTo(mail.getTo().split(","));
        val subject = mail.getSubject() != null ? mail.getSubject() : "";
        helper.setSubject(subject);
        helper.setText(mail.getBody());

        String filePath = mail.getFilePath();
        if (filePath != null && Files.exists(Paths.get(filePath))) {
            helper.addAttachment(mail.getFilename(), new File(filePath));
        }

        emailSender.send(message);
    }

    public void testMessage() throws MessagingException {
        EmailDto mail = new EmailDto("my_contact@bk.ru","test message","hello",null,null);
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom(new InternetAddress(properties.getFrom()));
        helper.setTo(mail.getTo().split(","));

        val subject = mail.getSubject() != null ? mail.getSubject() : "";
        helper.setSubject(subject);
        helper.setText(mail.getBody());

        String filePath = mail.getFilePath();
        if (filePath != null && Files.exists(Paths.get(filePath))) {
            helper.addAttachment(mail.getFilename(), new File(filePath));
        }

        emailSender.send(message);
    }

    @Scheduled(fixedDelay = 10000)
    public void test() throws MessagingException {
        System.out.println("***********************   "+properties.getFrom());
        testMessage();
    }

}
