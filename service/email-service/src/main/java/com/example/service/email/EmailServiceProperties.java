package com.example.service.email;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
//import org.springframework.boot.context.properties.ConfigurationProperties;

//@Configuration
//@ConfigurationProperties("emailservice")
@ConfigurationProperties(prefix = "emailservice")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class EmailServiceProperties {
    String from;//="test@mail.ri";

    @PostConstruct
    void init(){
        System.out.println("***********************   "+getFrom());
    }



}
